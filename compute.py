#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 11:37:42 2021

@author: claudio
"""

#import meshio
import numpy as np
#import scipy.sparse as ss
#from scipy.sparse.linalg import inv, spsolve
#from scipy.linalg import block_diag

from sksparse.cholmod import cholesky, CholmodTooLargeError

import matplotlib.pyplot as plt

import section_properties as sec

from datetime import datetime

#case_folder = [#'./05x05x06_hex20/', './05x10x12_hex20/', './05x15x15_hex20/',
               #'./05x20x20_hex20/', './05x20x24_hex20/', './05x25x25_hex20/',]
#               './05x25x30_hex20/', './05x40x40_hex20/', './05x40x48_hex20/', './10x50x60_hex20/']

#case_folder = ['./05x10x12_hex20/', './05x15x15_hex20/', './05x15x18_hex20/', './05x20x20_hex20/', './05x20x24_hex20/', 
#               './05x25x25_hex20/', './05x25x30_hex20/', './05x30x30_hex20/', './05x30x36_hex20/',
#case_folder = ['./10x10x12_hex20/', './10x15x15_hex20/', './10x15x18_hex20/', './10x20x20_hex20/', './10x20x24_hex20/',]

# case_folder = ['./05x05x06_hex20/', './05x10x12_hex20/', './05x15x15_hex20/', './05x15x18_hex20/']

case_folder = ['./02x20x20_hex20/', './02x25x25_hex20/', './02x30x30_hex20/', './02x40x40_hex20/',
               './02x40x50_hex20/', './02x48x60_hex20/', './02x50x50_hex20/', './02x60x60_hex20/', './02x64x64_hex20/']



a_space = np.linspace(0.0, 1.0, num = 11 )
EA_c  = None #np.zeros((len(a_space),len(case_folder)))
GAy_c = None #np.zeros_like(EA_c)
GAz_c = None #np.zeros_like(EA_c)
EJy_c = None #np.zeros_like(EA_c)
EJz_c = None #np.zeros_like(EA_c)
GJx_c = None #np.zeros_like(EA_c)

w = 0.2
h = 0.25
E = 2.06e11
nu = 0.27
G = E/(2*(1+nu))
A = w*h

l = 0.05
ea = np.array([1.0, 0.0, 0.0])

EA_th = E*A

GAy_th = 5/6*G*A
GAz_th = 5/6*G*A

EJy_th = E*1/12*w*h**3
EJz_th = E*1/12*h*w**3
# Roark Table 10.1 point 4

aa = h/2
bb = w/2

GJx_th = G*aa*bb**3*(16/3-3.36*bb/aa*(1-bb**4/(12*aa**4)))

cases_done = []

def prog_plot(prog):
        
    plt.figure(figsize=(16,20))
    plt.subplot(321)
    for i in range(np.shape(EA_c)[1]):
        plt.plot(a_space,EA_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[EA_th,EA_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$EA$');
    
    plt.subplot(322)
    for i in range(np.shape(GJx_c)[1]):
        plt.plot(a_space,GJx_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[GJx_th,GJx_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$GJ_x$');
    
    
    plt.subplot(323)
    for i in range(np.shape(GAy_c)[1]):
        plt.plot(a_space,GAy_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[GAy_th,GAy_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$GA_y$');
    
    plt.subplot(324)
    for i in range(np.shape(GAz_c)[1]):
        plt.plot(a_space,GAz_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[GAz_th,GAz_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$GA_z$');
    
    plt.subplot(325)
    for i in range(np.shape(EJy_c)[1]):
        plt.plot(a_space,EJy_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[EJy_th,EJy_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$EJ_y$');
    
    plt.subplot(326)
    for i in range(np.shape(EJz_c)[1]):
        plt.plot(a_space,EJz_c[:,i],label=cases_done[i][2:-1])
    plt.plot([0,1],[EJz_th,EJz_th],'-.',label='th')
    plt.legend()
    plt.xlim((0,1))
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$EJ_z$');
    plt.savefig('stiffness_02x_{}.jpg'.format(prog))



for i, case in enumerate(case_folder):
    
    print("")
    now = datetime.now()
    print(now)

    print(" ")
    print("Starting case " + case)
    print(" ")
    
    S = sec.SectionProperties()
    S.read_mesh(case+'modal_mat.rmed','med')
    S.get_interface_points()
    # S.mesh.point_tags
    # S.get_modes(1)
    S.load_matrices(case_folder[i]+'Mass.npz', case_folder[i]+'Stiff.npz')
    
    try:
        S.compute_CB()
    except CholmodTooLargeError:
        print("caught exception in CB method. Exiting loop...")
        break
    
    cases_done.append(case)
    
    # computation completed we add a row in each data

    if EA_c is None:
        EA_c = np.zeros((len(a_space),1))
        GAy_c = np.zeros_like(EA_c)
        GAz_c = np.zeros_like(EA_c)
        GJx_c = np.zeros_like(EA_c)
        EJy_c = np.zeros_like(EA_c)
        EJz_c = np.zeros_like(EA_c)
    else:
        EA_c = np.hstack([EA_c, np.zeros((len(a_space),1))])
        GAy_c = np.hstack([GAy_c, np.zeros((len(a_space),1))])
        GAz_c = np.hstack([GAz_c, np.zeros((len(a_space),1))])
        GJx_c = np.hstack([GJx_c, np.zeros((len(a_space),1))])
        EJy_c = np.hstack([EJy_c, np.zeros((len(a_space),1))])
        EJz_c = np.hstack([EJz_c, np.zeros((len(a_space),1))])
    
    S.compute_RB_H_matrix(np.array([0.,0.,0.]), np.array([l,0.,0.]) )
    # S.compute_H_hat(1.0)
    S.compute_MSE_correction_matrix()
    S.compute_H_hat(1.0)
    S.compute_mass_properties()
    
    print(S.mass)
    print(S.p_cm)
    print(S.Jcm)
    
    for j, a in enumerate(a_space):    
        S.compute_H_hat(a)
        S.compute_elastic_properties(l, ea)
        
        EA_c[j,i] = S.D[0,0]
        GAy_c[j,i] = S.D[1,1]
        GAz_c[j,i] = S.D[2,2]
        GJx_c[j,i] = S.D[3,3]
        EJy_c[j,i] = S.D[4,4]
        EJz_c[j,i] = S.D[5,5]


    prog_plot(i)
        
    #del S



