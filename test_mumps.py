#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:52:36 2021

@author: claudio
"""

"""
DMUMPS test routine.
Run as:
    mpirun -np 2 python dsimpletest.py
The solution should be [ 1. 2. 3. 4. 5.].
"""
import scipy.sparse as ss
import numpy as np
import mumps

import time

from datetime import datetime




#case_folder = ['./10x5x6_hex20/', './10x10x12_hex20/', './10x15x15_hex20/',
#               './10x20x20_hex20/', './10x25x25_hex20/', './10x25x30_hex20/',
#               './10x40x40_hex20/', './10x50x50_hex20/', './10x50x60_hex20/']


case_folder = './10x50x60_hex20/'




# Create the MUMPS context and set the array and right hand side
ctx = mumps.DMumpsContext(sym=0, par=1, comm=None)
if ctx.myid == 0:
    
    K_LL = ss.load_npz(case_folder+'K_LL.npz')
    
    tic = time.perf_counter()
    
    perm = ss.csgraph.reverse_cuthill_mckee(K_LL, symmetric_mode=True)
    
    K_LL1 = K_LL[np.ix_(perm, perm)]

    toc = time.perf_counter()
    
    print(f"Permutation computed in {toc - tic:0.4f} seconds")

    K_LR = ss.load_npz(case_folder+'K_LR.npz')
        
    #K_LR = K_LR.todense()

    K_LRt = K_LR[perm,0].todense()

    print(" ")
    now = datetime.now()
    print(now)
        
    tic = time.perf_counter()

    
    
    ctx.set_centralized_sparse(K_LL1)
    x = K_LRt.copy()
    ctx.set_rhs(x)

ctx.set_silent() # Turn off verbose output

ctx.run(job=6) # Analysis + Factorization + Solve

if ctx.myid == 0:
    print("Solution is %s." % (x,))
    toc = time.perf_counter()
    print(f"Phi_r computed in {toc - tic:0.4f} seconds")

ctx.destroy() # Free memory



