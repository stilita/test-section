#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 15:41:25 2021

@author: claudio
"""

import numpy as np
import meshio
import scipy.sparse as ss


case_folder = ['./10x5x6_hex20/', './10x10x12_hex20/', './10x15x15_hex20/',
               './10x20x20_hex20/', './10x25x25_hex20/', './10x25x30_hex20/',
               './10x40x40_hex20/', './10x50x50_hex20/', './10x50x60_hex20/']

#case_folder = ['./10x20x20_hex20/']


for i, case in enumerate(case_folder):
    
    print('Reading mesh for case '+case)
    mesh = meshio.read(case+'modal_mat.rmed','med')

    for k in mesh.point_tags:
        if mesh.point_tags[k][0] == 'R1':
            r1_tag_value = k
        elif mesh.point_tags[k][0] == 'R2':
            r2_tag_value = k
                
    #indexes are related to points
        
    #get point indexes belonging to R1
    r1_set_point = mesh.point_data['point_tags'] == r1_tag_value

    #make array of indexes where points belong to R1
    r1_set_point_ix = np.where(r1_set_point)[0]

    #get point indexes belonging to R2
    r2_set_point = mesh.point_data['point_tags'] == r2_tag_value

    #make array of indexes where points belong to R2
    r2_set_point_ix = np.where(r2_set_point)[0]

    #get point indexes belonging to R set (R1 or R2)
    r_set_point = np.logical_or(r1_set_point, r2_set_point)

    #get point indexes belonging to L set not (R1 or R2)
    l_set_point = np.logical_not(r_set_point)

    #repeat each value 3 times for coordinates
    r1_set = np.repeat(r1_set_point,3)
    r2_set = np.repeat(r2_set_point,3)
    r_set  = np.repeat(r_set_point,3)
    l_set  = np.repeat(l_set_point,3)
        
    # make array of indexes where coordinates belong to r1 set
    r1_set_ix = np.where(r1_set)[0]

    # make array of indexes where coordinates belong to r2 set
    r2_set_ix = np.where(r2_set)[0]
        
    # make array of indexes where coordinates belong to r set
    # r_set_ix = np.where(r_set)[0]
    r_set_ix = np.concatenate((r1_set_ix, r2_set_ix))
        
    # make array of indexes where coordinates belong to l set
    l_set_ix = np.where(l_set)[0]
        
    # count size of coordinates belonging to r1 set
    r1_set_num = np.sum(r1_set)

    # count size of coordinates belonging to r2 set
    r2_set_num = np.sum(r2_set)

    # count size of coordinates belonging to r set
    r_set_num = np.sum(r_set)

    # count size of coordinates belonging to l set
    l_set_num = np.sum(l_set)
    
    print('Reading matrices...')
    
    M_tot = ss.load_npz(case+'Mass.npz')
    K_tot = ss.load_npz(case+'Stiff.npz')
    
    # M_RR = M_tot[np.ix_(r_set_ix, r_set_ix)]

    M_LL = M_tot[np.ix_(l_set_ix, l_set_ix)]

    # M_RL = M_tot[np.ix_(r_set_ix, l_set_ix)]
        
    # M_LR = M_tot[np.ix_(l_set_ix, r_set_ix)]
        
    # K_RR = K_tot[np.ix_(r_set_ix, r_set_ix)]

    K_LL = K_tot[np.ix_(l_set_ix, l_set_ix)]

    # K_RL = K_tot[np.ix_(r_set_ix, l_set_ix)]

    K_LR = K_tot[np.ix_(l_set_ix, r_set_ix)]
    
    print('Saving M_LL')
    ss.save_npz(case+'M_LL.npz', M_LL)

    print('Saving K_LL')
    ss.save_npz(case+'K_LL.npz', K_LL)
    
    print('Saving K_LR')
    ss.save_npz(case+'K_LR.npz', K_LR)
        
