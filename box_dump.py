#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.7.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/Section/test-section')

####################################################
##       Begin of NoteBook variables section      ##
####################################################
notebook.set("x0", 0)
notebook.set("x1", 0.05)
notebook.set("y0", -0.1)
notebook.set("y1", 0.1)
notebook.set("z0", -0.125)
notebook.set("z1", 0.125)

nsx = 2
nsy = 64
nsz = 64

notebook.set("n_seg_x", nsx)
notebook.set("n_seg_y", nsy)
notebook.set("n_seg_z", nsz)

mesh_name = 'section_{0:02d}x{1:02d}x{2:02d}_hex20'.format(nsx, nsy, nsz)

is_quadratic = True
is_biquadratic = False

####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Vertex_1 = geompy.MakeVertex("x0", "y0", "z0")
Vertex_2 = geompy.MakeVertex("x1", "y1", "z1")
Box_1 = geompy.MakeBoxTwoPnt(Vertex_1, Vertex_2)
[z, y, x] = geompy.Propagate(Box_1)
R1 = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [3])
R2 = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [13])
all = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(all, [3, 13, 23, 27, 31, 33])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Box_1, 'Box_1' )
geompy.addToStudyInFather( Box_1, z, 'z' )
geompy.addToStudyInFather( Box_1, y, 'y' )
geompy.addToStudyInFather( Box_1, x, 'x' )
geompy.addToStudyInFather( Box_1, R1, 'R1' )
geompy.addToStudyInFather( Box_1, R2, 'R2' )
geompy.addToStudyInFather( Box_1, all, 'all' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

section = smesh.Mesh(Box_1)
Hexa_3D = section.Hexahedron(algo=smeshBuilder.Hexa)
Quadrangle_2D = section.Quadrangle(algo=smeshBuilder.QUADRANGLE,geom=all)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])
Regular_1D = section.Segment(geom=z)
nz = Regular_1D.NumberOfSegments("n_seg_z",None,[])
Regular_1D_1 = section.Segment(geom=y)
ny = Regular_1D_1.NumberOfSegments("n_seg_y",None,[])
Regular_1D_2 = section.Segment(geom=x)
nx = Regular_1D_2.NumberOfSegments("n_seg_x",None,[])
R1_1 = section.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_1 = section.GroupOnGeom(R2,'R2',SMESH.NODE)
isDone = section.Compute()

if is_quadratic:
    section.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)

smesh.SetName(section, 'section')
try:
  section.ExportMED(r'/home/claudio/Projects/Studies/Section/'+mesh_name+'.med',auto_groups=0,version=40,overwrite=1,meshPart=None,autoDimension=1)
  print("MED file saved")
except:
  print('ExportMED() failed. Invalid file name?')



## Set names of Mesh objects
smesh.SetName(R2_1, 'R2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(section.GetMesh(), 'section')
smesh.SetName(ny, 'ny')
smesh.SetName(nz, 'nz')
smesh.SetName(nx, 'nx')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
