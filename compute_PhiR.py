#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 16:02:13 2021

@author: claudio
"""

import numpy as np
import scipy.sparse as ss
import scipy.sparse.linalg as ssla
#import numpy.linalg as npla

from sksparse.cholmod import cholesky

import matplotlib.pyplot as plt

import time

from datetime import datetime



#case_folder = ['./10x5x6_hex20/', './10x10x12_hex20/', './10x15x15_hex20/',
#               './10x20x20_hex20/', './10x25x25_hex20/', './10x25x30_hex20/',
#               './10x40x40_hex20/', './10x50x50_hex20/', './10x50x60_hex20/']

case_folder = ['./10x20x20_hex20/', './10x25x25_hex20/', './10x25x30_hex20/', './10x40x40_hex20/']


for i, case in enumerate(case_folder):
    
    print(" ")
    print("Analyzing case: "+case+"...")
    print(" ")
    
    print('Read matrices')
    
    K_LL = ss.load_npz(case+'K_LL.npz')
    K_LL = K_LL.tocsc()
    
    K_LR = ss.load_npz(case+'K_LR.npz')
        
    K_LR = K_LR.tocsc()
    
    #plt.figure(1)
    #plt.spy(K_LL)    

    print("")
    now = datetime.now()
    print(now)
    
    tic = time.perf_counter()
    
    perm = ss.csgraph.reverse_cuthill_mckee(K_LL, symmetric_mode=True)
    
    K_LL1 = K_LL[np.ix_(perm, perm)]
    
    K_LR1 = K_LR[perm,:]
    
    toc = time.perf_counter()
    print(f"Permutation computed in {toc - tic:0.4f} seconds")
    
    #plt.figure(2)
    #plt.spy(K_LL1)
    
    
    if True:
        now = datetime.now()
        print(now)
        print("start computing Phi_r using Cholesky")
        
        tic = time.perf_counter()
        
    
        phi_r1 = np.zeros_like(K_LR)
        factor = cholesky(K_LL1)
        phi_r1 = -factor.solve_A(K_LR1).todense() 

        toc = time.perf_counter()
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")
    
    if False:
        now = datetime.now()
        print(now)
        print("start computing Phi_r with spsolve column by column with colamnd and umfpack")
        
        tic = time.perf_counter()
        
    
        phi_r = np.zeros_like(K_LR)
        
        mean_col = []
        
        for k in range(np.shape(phi_r)[1]):
            tic1 = time.perf_counter()
            res = ssla.spsolve(K_LL, K_LR[:,k], permc_spec='COLAMD', use_umfpack=True)
            phi_r[:,k] = -res[:, np.newaxis]
            toc1 = time.perf_counter()
            
            mean_col.append(toc1-tic1)
        
        
        print('avg. column computation in {0} sec'.format(np.mean(mean_col)))
    
        toc = time.perf_counter()
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")

    if False:
        print(" ")
        now = datetime.now()
        print(now)
        print("start computing Phi_r with spsolve called once")
        
        tic = time.perf_counter()
        
        phi_r = np.zeros_like(K_LR1)
        
        phi_r = -ssla.spsolve(K_LL1, K_LR1, permc_spec='COLAMD', use_umfpack=True).todense()
        
        toc = time.perf_counter()
    
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")
        
        print("same results? {0}".format(np.allclose(phi_r,phi_r1)))

    if False:
        print(" ")
        now = datetime.now()
        print(now)
        print("start computing Phi_r with spsolve called once on permuted KLL")
       
        tic = time.perf_counter()
       
        phi_r = np.zeros_like(K_LR)
       
        phi_r = -ssla.splu(K_LL1).solve(K_LR1)
        #-ssla.spsolve(K_LL1, K_LR1, permc_spec=None, use_umfpack=True)
       
        toc = time.perf_counter()
   
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")


    
    if False:
        now = datetime.now()
        print(now)
        print(" ")
        print("start computing Phi_r with inverse")
        
        tic = time.perf_counter()
           
        phi_r = np.zeros_like(K_LR)
    
        phi_r = -ssla.inv(K_LL).dot(K_LR)
    
        toc = time.perf_counter()
    
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")
    
    if False:
        now = datetime.now()
        print(now)
        print(" ")
        print("start computing Phi_r with pseudoinverse")
        
        tic = time.perf_counter()
        
        phi_r = np.zeros_like(K_LR)
        
        pinv = ssla.spilu(K_LL1, drop_tol=1e-06)
        
        phi_r = -(pinv.solve(K_LR1))
    
        toc = time.perf_counter()
    
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")
    
    if False:
        now = datetime.now()
        print(now)
        print("start computing Phi_r with bicgstab column by column")
        
        tic = time.perf_counter()
           
        phi_r = np.zeros_like(K_LR)
        
        mean_col = []
        
        # scipy.sparse.linalg.bicgstab(A, b, x0=None, tol=1e-05, maxiter=None, M=None, callback=None
        for k in range(np.shape(phi_r)[1]):
            
            tic1 = time.perf_counter()
            
            KLL_iLU = ssla.spilu(K_LL)
            M = ssla.LinearOperator(K_LL.shape, KLL_iLU.solve)
            
            res, status = ssla.bicgstab(K_LL, K_LR[:,k], x0 = None, tol=1e-08, maxiter=1000, M=M, callback=None )
            
            if status == 0:
                phi_r[:,k] = -res[:, np.newaxis]
            elif status > 0:
                phi_r[:,k] = -res[:, np.newaxis]
                print("Warning! maximum iterations reached")
            else:
                print("Error!")
                
            toc1 = time.perf_counter()
            
            mean_col.append(toc1-tic1)
            
        print('avg. column computation in {0} sec'.format(np.mean(mean_col)))
            
        toc = time.perf_counter()
    
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")
        
    
    

    #self.phi_r = -npla.solve(tmp_K_LL, tmp_K_LR)
    
    
    
    
    #M_RL_phi_R = M_RL.dot(phi_r)
    
    #print("compute M_BB")
    
    #tic = time.perf_counter()
    #Mat1 = (self.phi_r.T)*self.M_LL*self.phi_r 

    #toc = time.perf_counter()
    #print(f"Step 1 {toc - tic:0.4f} seconds")
    
    #tic1 = toc
    #Mat2 = M_RL_phi_R.T + Mat1

    #toc = time.perf_counter()
    #print(f"Step 2 {toc - tic1:0.4f} seconds")
    
    #tic2 = toc
    
    #Mat3 = M_RL_phi_R + Mat2
    
    #toc = time.perf_counter()
    #print(f"Step 3 {toc - tic2:0.4f} seconds")
    
    #tic3 = toc
    
    #self.M_BB = self.M_RR + Mat3

    #toc = time.perf_counter()
    #print(f"Step 4 {toc - tic3:0.4f} seconds")
    #print(f"M_BB computed in {toc - tic:0.4f} seconds")