#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 12:28:17 2021

@author: claudio
"""

import numpy as np
import meshio
import scipy.sparse as ss
import scipy.sparse.linalg as ssla
import numpy.linalg as npla
from scipy.linalg import block_diag

from sksparse.cholmod import cholesky, CholmodTooLargeError


import time


class SectionProperties:
    def __init__(self, ):
        self.mesh = None
        self.computed = False
    
    def read_mesh(self, meshname, meshformat=None):
        if meshformat is not None:
            self.mesh = meshio.read(meshname, meshformat)
        else:
            try:
                self.mesh = meshio.read(meshname)
            except Exception:
                print(Exception)
    
    def get_interface_points(self, names = ['R1', 'R2']):
        
        for k in self.mesh.point_tags:
            if self.mesh.point_tags[k][0] == names[0]:
                self.r1_tag_value = k
            elif self.mesh.point_tags[k][0] == names[1]:
                self.r2_tag_value = k
                
        #indexes are related to points
        
        #get point indexes belonging to R1
        self.r1_set_point = self.mesh.point_data['point_tags'] == self.r1_tag_value

        #make array of indexes where points belong to R1
        self.r1_set_point_ix = np.where(self.r1_set_point)[0]

        #get point indexes belonging to R2
        self.r2_set_point = self.mesh.point_data['point_tags'] == self.r2_tag_value

        #make array of indexes where points belong to R2
        self.r2_set_point_ix = np.where(self.r2_set_point)[0]

        #get point indexes belonging to R set (R1 or R2)
        self.r_set_point = np.logical_or(self.r1_set_point, self.r2_set_point)

        #get point indexes belonging to L set not (R1 or R2)
        self.l_set_point = np.logical_not(self.r_set_point)

        #repeat each value 3 times for coordinates
        self.r1_set = np.repeat(self.r1_set_point,3)
        self.r2_set = np.repeat(self.r2_set_point,3)
        self.r_set  = np.repeat(self.r_set_point,3)
        self.l_set  = np.repeat(self.l_set_point,3)
        
        # make array of indexes where coordinates belong to r1 set
        self.r1_set_ix = np.where(self.r1_set)[0]

        # make array of indexes where coordinates belong to r2 set
        self.r2_set_ix = np.where(self.r2_set)[0]
        
        # make array of indexes where coordinates belong to r set
        # r_set_ix = np.where(r_set)[0]
        self.r_set_ix = np.concatenate((self.r1_set_ix, self.r2_set_ix))
        
        # make array of indexes where coordinates belong to l set
        self.l_set_ix = np.where(self.l_set)[0]
        
        # count size of coordinates belonging to r1 set
        self.r1_set_num = np.sum(self.r1_set)

        # count size of coordinates belonging to r2 set
        self.r2_set_num = np.sum(self.r2_set)

        # count size of coordinates belonging to r set
        self.r_set_num = np.sum(self.r_set)

        # count size of coordinates belonging to l set
        self.l_set_num = np.sum(self.l_set)
    
    def get_modes(self, number):
        self.omega2 = []
        
        self.num_modes = number
        
        if True:
            self.get_modes_from_MED()
        else:
            print("Not yet implemented")


    def get_modes_from_MED(self):
        
        mode_keys = list(self.mesh.point_data.keys())
        self.phi_l = np.zeros((self.l_set_num, self.num_modes))
    
        k=0

        while k<self.num_modes:
            result = [i for i in mode_keys if i.startswith('modes___DEPL['+str(k)+']')]
            if result:
                #print(result)
                curr_f = float(result[0].split(' ')[-1])
                #print(curr_f)
                self.omega2.append((2*np.pi*curr_f)**2)
                curr_mode = np.ravel(self.mesh.point_data[result[0]],'C')[self.l_set]
                self.phi_l[:,k] = curr_mode
            else:
                break
            k+=1
            
    def load_matrices(self, M_file, K_file):
        
        self.M_tot = ss.load_npz(M_file)
        self.K_tot = ss.load_npz(K_file)
        
        self.M_RR = self.M_tot[np.ix_(self.r_set_ix, self.r_set_ix)]
        #print(M_RR.shape)

        self.M_LL = self.M_tot[np.ix_(self.l_set_ix, self.l_set_ix)]
        #print(M_LL.shape)

        self.M_RL = self.M_tot[np.ix_(self.r_set_ix, self.l_set_ix)]
        #print(M_RL.shape)
        
        self.M_LR = self.M_tot[np.ix_(self.l_set_ix, self.r_set_ix)]
        #print(M_LR.shape)
        
        self.K_RR = self.K_tot[np.ix_(self.r_set_ix, self.r_set_ix)]
        #print(K_RR.shape)

        self.K_LL = self.K_tot[np.ix_(self.l_set_ix, self.l_set_ix)]
        #print(K_LL.shape)

        self.K_RL = self.K_tot[np.ix_(self.r_set_ix, self.l_set_ix)]
        #print(K_RL.shape)

        self.K_LR = self.K_tot[np.ix_(self.l_set_ix, self.r_set_ix)]
        #print(K_LR.shape)

    def compute_CB(self):
        print("start computing Phi_r")
        
        tic = time.perf_counter()
        
        
        # stra lentissimo
        #self.phi_r = -ssla.inv(self.K_LL.tocsc()).dot(self.K_LR.tocsc())
        
        
        if False:
            perm = ss.csgraph.reverse_cuthill_mckee(self.K_LL, symmetric_mode=True)
            tmp_K_LL = self.K_LL[np.ix_(perm, perm)].tocsc()
            tmp_K_LR = self.K_LR[perm,:].tocsc()
        else:
            tmp_K_LL = self.K_LL.tocsc()
            tmp_K_LR = self.K_LR.tocsc()
            
        
        #self.phi_r = np.zeros_like(tmp_K_LR)
        
        #self.phi_r = np.zeros_like(K_LR)
        
        if False:
            self.phi_r = -ssla.spsolve(tmp_K_LL, tmp_K_LR, permc_spec='COLAMD', use_umfpack=True).todense()
        else:
            try:
                factor = cholesky(tmp_K_LL, ordering_method='colamd') #, use_long=True )
                self.phi_r = -factor.solve_A(tmp_K_LR).todense() 
            except CholmodTooLargeError:
                print("Problem seems too large :( ")
                raise

        #self.phi_r = -npla.solve(tmp_K_LL, tmp_K_LR)
        
        #prec = ssla.spilu(tmp_K_LL, drop_tol=1e-04)


        # scipy.sparse.linalg.bicgstab(A, b, x0=None, tol=1e-05, maxiter=None, M=None, callback=None
        #for k in range(tmp_K_LR.shape[1]):
        #    res, status = ssla.bicgstab(tmp_K_LL, tmp_K_LR[:,k], x0 = None, tol=1e-08, maxiter=1000, M=None, callback=None )
        #    
        #    if status == 0:
        #        self.phi_r[:,k] = -res[:, np.newaxis]
        #    elif status > 0:
        #        self.phi_r[:,k] = -res[:, np.newaxis]
        #        print("Warning! maximum iterations reached")
        #    else:
        #        print("Error!")
        
        toc = time.perf_counter()
        print(f"Phi_r computed in {toc - tic:0.4f} seconds")

        M_RL_phi_R = self.M_RL.dot(self.phi_r)
        
        print("compute M_BB")
        
        tic = time.perf_counter()
        Mat1 = (self.phi_r.T).dot(self.M_LL.dot(self.phi_r)) 

        toc = time.perf_counter()
        print(f"Step 1 {toc - tic:0.4f} seconds")
        
        tic1 = toc
        Mat2 = M_RL_phi_R.T + Mat1

        toc = time.perf_counter()
        print(f"Step 2 {toc - tic1:0.4f} seconds")
        
        tic2 = toc
        
        Mat3 = M_RL_phi_R + Mat2
        
        toc = time.perf_counter()
        print(f"Step 3 {toc - tic2:0.4f} seconds")
        
        tic3 = toc
        
        self.M_BB = self.M_RR + Mat3

        toc = time.perf_counter()
        print(f"Step 4 {toc - tic3:0.4f} seconds")
        print(f"M_BB computed in {toc - tic:0.4f} seconds")
        
        tic = time.perf_counter()
   
        self.K_BB = self.K_RR + self.K_RL.dot(self.phi_r)
        
        toc = time.perf_counter()

        print(f"K_BB computed in {toc - tic:0.4f} seconds")
        
        # compute dense version of K_BB
        self.K_BB_d = self.K_BB #.todense()
        
    def check_MLL(self):
        Mtest = self.phi_l.T.dot(self.M_LL.dot(self.phi_l))
        print(Mtest)
        
    def check_KLL(self):
        Mtest = self.phi_l.T.dot(self.K_LL.dot(self.phi_l))
        print(Mtest)
        
    def compute_RB_H_matrix(self, point01, point02):
        
        self.p01 = point01
        self.p02 = point02
        
        self.H1 = np.zeros((self.r1_set_num,6))
        self.H2 = np.zeros((self.r2_set_num,6))
        
        for curr_point_index, curr_mesh_index  in enumerate(self.r1_set_point_ix):
            self.H1[3*curr_point_index:3*(curr_point_index+1),0:3] = np.eye(3)
            curr_point_coord = self.mesh.points[curr_mesh_index,:]
            #print("current index: {0}".format(curr_point_index))
            #print("current point index in mesh: {0}".format(curr_mesh_index))
            #print(curr_point_coord)
            #print(p01)
            self.H1[3*curr_point_index:3*(curr_point_index+1),3:] = np.cross(np.eye(3), (self.p01-curr_point_coord))
            #print(H1[3*curr_point_index:3*(curr_point_index+1),:])
            
        for curr_point_index, curr_mesh_index  in enumerate(self.r2_set_point_ix):
            self.H2[3*curr_point_index:3*(curr_point_index+1),0:3] = np.eye(3)
            curr_point_coord = self.mesh.points[curr_mesh_index,:]
            #print("current index: {0}".format(curr_point_index))
            #print("current point index in mesh: {0}".format(curr_mesh_index))
            #print(curr_point_coord)
            #print(p02)
            self.H2[3*curr_point_index:3*(curr_point_index+1),3:] = np.cross(np.eye(3), (self.p02-curr_point_coord))
            #print(H2[3*curr_point_index:3*(curr_point_index+1),:])
            
        #self.H = ss.block_diag((ss.csr_matrix(self.H1), ss.csr_matrix(self.H2)), format="csr")
        self.H = block_diag(self.H1, self.H2)
    
    def compute_C1C2(self):

        tic = time.perf_counter()

        self.Q1, self.R1 = np.linalg.qr(self.H1, mode='complete')
        self.Q2, self.R2 = np.linalg.qr(self.H2, mode='complete')

        toc = time.perf_counter()
        print(f"QR computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        self.C1 = self.Q1[:, 6:]
        self.C2 = self.Q2[:, 6:]
        
        self.C = block_diag(self.C1, self.C2)

        toc = time.perf_counter()
        print(f"C computed in {toc - tic:0.4f} seconds")

        
    def compute_Call(self):

        tic = time.perf_counter()

        self.Qall, self.Rall = np.linalg.qr(self.H, mode='complete')

        toc = time.perf_counter()
        print(f"QR (all) computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        self.Call = self.Qall[:,12:]

        toc = time.perf_counter()
        print(f"C (all) computed in {toc - tic:0.4f} seconds")

        
    def compute_MSE(self):
        
        print("start computing MSE correction")
        tic = time.perf_counter()

        
        V1 = self.K_BB_d.dot(self.H)

        toc = time.perf_counter()
        print(f"V1 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()


        V2 = (self.C.T).dot(V1)

        toc = time.perf_counter()
        print(f"V2 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()
        
        K21 = self.C.T @ self.K_BB_d
        
        K2 =  K21  @ self.C

        toc = time.perf_counter()
        print(f"K2 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        V3 = np.linalg.inv(K2) @ V2

        toc = time.perf_counter()
        print(f"V3 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        self.MSE_H = self.C @ V3

        toc = time.perf_counter()
        print(f"MSE_H computed in {toc - tic:0.4f} seconds")

    def compute_MSEall(self):
        
        print("start computing MSE correction")
        tic = time.perf_counter()

        
        V1 = self.K_BB_d.dot(self.H)

        toc = time.perf_counter()
        print(f"V1 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        V2all = (self.Call.T).dot(V1)

        toc = time.perf_counter()
        print(f"V2 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()
        
        K21all = self.Call.T @ self.K_BB_d
        
        K2all =  K21all  @ self.Call

        toc = time.perf_counter()
        print(f"K2 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        V3 = np.linalg.inv(K2all) @ V2all

        toc = time.perf_counter()
        print(f"V3 computed in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()

        self.MSE_Hall = self.Call @ V3

        toc = time.perf_counter()
        print(f"MSE_H computed in {toc - tic:0.4f} seconds")
        
        
    def compute_H_hat(self, mat, alpha = 1.0):

        #print("start computing H_hat")
        
        #tic = time.perf_counter()
        
        self.H_hat = self.H - alpha*mat

        #toc = time.perf_counter()
        #print(f"H_hat computed in {toc - tic:0.4f} seconds")

        
    def compute_mass_properties(self):
        
        Mr_11 = self.H_hat.T.dot(self.M_BB.dot(self.H_hat))
        
        Z_Rrb = np.zeros((12,6))
        Z_Rrb[0:3,0:3] = np.eye(3)
        Z_Rrb[3:6,3:6] = np.eye(3)
        Z_Rrb[6:9,0:3] = np.eye(3)
        Z_Rrb[9:12,3:6] = np.eye(3)
        #Z_Rrb[0:3,3:6] = np.cross(np.eye(3), (p01))
        Z_Rrb[6:9,3:6] = np.cross(np.eye(3), -(self.p02-self.p01))
        
        Mrb = Z_Rrb.T.dot(Mr_11.dot(Z_Rrb))
        
        self.mass = Mrb[0:3,0:3]
        self.Scm  = Mrb[3:6,0:3]
        self.Jref = Mrb[3:6,3:6]
        
        self.p_cm = np.zeros((3,1))
        S_cm = np.zeros((3,1))
        
        S_cm[0] = (Mrb[5,1]-Mrb[4,2])/2
        S_cm[1] = (Mrb[3,2]-Mrb[5,0])/2
        S_cm[2] = (Mrb[4,0]-Mrb[3,1])/2

        self.p_cm = S_cm/Mrb[0,0]
        
        self.Jcm = self.Jref-Mrb[0,0]*np.eye(3)*(self.p_cm.T.dot(self.p_cm)-np.outer(self.p_cm, self.p_cm))
        
    def compute_elastic_properties(self, l, ea, corrected=False):
        
        if corrected:
            Kr = (self.H.T).dot(self.K_BB_d.dot(self.H_hat))
        else:
            Kr = (self.H_hat.T).dot(self.K_BB_d.dot(self.H_hat))
        
        el_mat = np.linalg.inv(Kr[6:,6:])
        
        A = el_mat[0:3,0:3]
        B = el_mat[0:3,3:6]
        Bt = el_mat[3:6,0:3]
        E = el_mat[3:6,3:6]
        
        ea_cross = np.cross(np.eye(3),ea)
        
        Gmm = 1/l*E
        Gfm = 1/l*B - 1/2*ea_cross.T.dot(E)
        Gff = 1/l*A-1/2*(ea_cross.T.dot(Bt) + B.dot(ea_cross))+l/6*ea_cross.T.dot(E.dot(ea_cross))
        
        G = np.block([[Gff, Gfm], [Gfm.T, Gmm]])
        
        self.D = np.linalg.inv(G)
        